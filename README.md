# Zinso Obvious Minimal Language

[![Crates.io](https://img.shields.io/crates/v/zoml.svg)](https://crates.io/crates/zoml)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/zoml)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/zoml/-/raw/master/LICENSE)

# ZOML
[Zinso Obvious Minimal Language]

A user-friendly configuration file format.

ZOML strives to be a simplistic configuration file format that is highly readable, thanks to its clear and intuitive meanings. ZOML is specifically designed to seamlessly translate into a window without any ambiguity. Additionally, ZOML aims to be effortlessly parsed into various data structures across different programming languages.

```d
//comment
///comment
/*
comment
*/
```

```d
title = "config.zoml Example"
languages = [ 'Ruby', 'Perl', 'Python']
websites = {
    ZOML: 'zoml.io',
    Ruby: 'ruby-lang.org',
    Python: 'python.org',
    Perl: 'use.perl.org',
}
students = {
    class_1:["andrew","ryan","gory","klora"],
    class_2:["roin","rok","kate","jem"],
}
```
![Zoml](https://gitcode.net/dnrops/blog_images/-/raw/main/all_imgs/Zoml.webp)

## ZOML includes helpful built-in data types
 - Key/Value Pairs
 - Maps
 - Arrays
 - Integers & Floats
 - Booleans
## ZOML has broad language support

ZOML is widely embraced by the programming community and has implementations available in Rust for now.And in the future will be implemented in C, C#, C++, Clojure, Dart, Elixir, Erlang, Go, Haskell, Java, JavaScript, Lua, Objective-C, Perl, PHP, Python, Ruby, Swift, Scala, and more. This extensive language compatibility ensures that ZOML can be seamlessly integrated into projects written in different programming languages, catering to a diverse range of developers.
